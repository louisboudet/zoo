# ZOO #

Sur une map existante, créez votre personnage animé.
Techno libre :)

### Comment ça marche ? ###

* installez git sur votre ordinateur si vous ne l'avez pas (https://git-scm.com/)
* sur votre ordi, créez un dossier zoo et dedans un dossier avec votre prénom sans accent, sans majuscule (ex: zoo/louis/)
* allez dans le dossier 'zoo' avec un terminal (rechercher cmd sur Windows et terminal sur MacOs ou Linux)
* aller dans l'onglet "source" du repo bitbucket et cliquer sur le bouton `[clone]`
* copier la ligne de commande `git clone …`
* une fois les sources récupérées, vous pouvez commencer à travailler.
* Quand une modification peut être _versionnée_ procédez comme suit :
* `git remote add origin https://<USER.NAME>@bitbucket.org/louisboudet/zoo.git` pour ajouter l'origine en ligne à votre git local (en remplaçant <USER.NAME> par votre nom d'utilisateur)
* `git config branch.master.remote origin` pour implémenter la branche master par défaut
* `git config branch.master.merge refs/heads/master` pour configurer la branche principale (ici master)
* `git status` pour vérifer les modifications en cours
* `git add <filename>` (sans les chevrons) pour écouter les fichier un à un
* `git pull`pour récupérer les modifs du repo faites par d'autres utilisateurs
* `git add .` pour écouter l'ensemble des fichiers modifiés
* `git commit -m "<votre message de mise à jour>"`
* pour identifier les messages de commit on utilisera les conventions de nommage suivantes (nomenclature complète ici https://www.conventionalcommits.org/en/v1.0.0/) :
	+ `feat: ` (_=feature_) pour une nouvelle fonctionnalité
	+ `fix: ` pour une correction
	+ `style: ` pour des modifications de css uniquement
	+ `refactor: ` en cas de renommage de fichiers
* les commits peuvent être très nombreux, ils servent à baliser les étapes de développement du projet
* `git push` pour envoyer vos commits et donc mettre à dispo votre code sur le repo pour tous les utilisateurs
* l'objectif est de jouer avec l'outil pour comprendre son fonctionnment et son utilité en cas de travail en équipe

### Qu'est ce qu'il faut faire ? ###

* créer un animal (j'en appelle à vos références ou à votre imagination)
* il faut l'animer en boucle (il faudra donc du JS)
* le fichier index.html à la racine de votre dossier "prenom" devra commencer par `<div class="prenom">`
* et TOUTES vos déclarations css devront comporter la class `.prenom` en début de ligne
* si du JS est utilisé, il faudra également appliquer cette règle

### Il y a des contraintes ? ###

* l'animal doit être entièrement en css ou en svg
* taille de la `<div class="prenom">` doit être entre 800px et 1000px de large pour entre 600px et 1000px de haut
* pour le reste on s'arrange ensemble
* si votre animal est une girafe, sa tête peut sortir du cadre (dans ce cas, venez m'en parler on mettra en place une pirouette pour que vous puissiez travailler dans de bonnes conditions et que ça n'impacte pas la map)


_le premier qui écrit "prenom" au lieu de son vrai prénom se fait virer du loft_
