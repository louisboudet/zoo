<!DOCTYPE html>
<html lang="en" dir="ltr">
	<head>
		<meta charset="utf-8">
		<title>ZOO B3</title>
		<link rel="stylesheet" href="./assets/css/styles.min.css">
	</head>
	<body>
		<main>
			<div class="zoo-wrapper">
				<div class="enclos enclos-anais"><?php include("./anais/index.html"); ?></div>
				<div class="enclos enclos-audrey"><?php include("./audrey/index.html"); ?></div>
				<div class="enclos enclos-clement"><?php include("./clement/index.html"); ?></div>
				<div class="enclos enclos-dorian"><?php include("./dorian/index.html"); ?></div>
				<div class="enclos enclos-emma"><?php include("./emma/index.html"); ?></div>
				<div class="enclos enclos-fabian"><?php include("./fabian/index.html"); ?></div>
				<div class="enclos enclos-marie"><?php include("./marie/index.html"); ?></div>
				<div class="enclos enclos-isaline"><?php include("./isaline/index.html"); ?></div>
				<div class="enclos enclos-shailash"><?php include("./shailash/index.html"); ?></div>
				<div class="enclos enclos-yohan"><?php include("./yohan/index.html"); ?></div>
			</div>
		</main>
	</body>
</html>
